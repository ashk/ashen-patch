DELETE FROM command WHERE name = 'push event';
INSERT INTO `command` (`name` ,`security` ,`help`) VALUES
('push event', '3', 'Syntax: .push event [idmin] [idmax] Charge des EVENT depuis la bdd.');

DELETE FROM `trinity_string` WHERE `entry` BETWEEN 12020 AND 12021;
INSERT INTO `trinity_string` (`entry` ,`content_default`) VALUES
('12120', 'Aucun event de trouv� entre %d et %d'),
('12121', 'Les event %d � %d ont �t� charg�s');

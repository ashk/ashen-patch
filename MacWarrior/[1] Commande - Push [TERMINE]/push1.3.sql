DELETE FROM command WHERE name IN ('push npc', 'push gob', 'push item');
INSERT INTO `command` (`name` ,`security` ,`help`) VALUES
('push npc', '3', 'Syntax: .push npc [idmin] [idmax] Charge des NPC depuis la bdd.'),
('push gob', '3', 'Syntax: .push gob [idmin] [idmax] Charge des GOB depuis la bdd.'),
('push item', '3', 'Syntax: .push item [idmin] [idmax] Charge des ITEM depuis la bdd.');

DELETE FROM `trinity_string` WHERE `entry` BETWEEN 12000 AND 12007;
INSERT INTO `trinity_string` (`entry` ,`content_default`) VALUES
('12110', 'Le premier argument doit �tre un ID'),
('12111', 'Vous devez entrer au moins un ID'),
('12112', 'Aucune cr�ature de trouv�e entre %d et %d'),
('12113', 'Les cr�atures %d � %d ont �t� charg�es'),
('12114', 'Aucun gameobject de trouv� entre %d et %d'),
('12115', 'Les gameobjects %d � %d ont �t� charg�s'),
('12116', 'Aucun item de trouv� entre %d et %d'),
('12117', 'Les items %d � %d ont �t� charg�s');

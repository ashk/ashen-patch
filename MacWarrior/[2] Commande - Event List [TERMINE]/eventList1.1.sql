ALTER TABLE `game_event`
	ADD `event_list` BOOLEAN NOT NULL DEFAULT '0' COMMENT 'Apparition de l''event dans la commande .event list';

DELETE FROM command WHERE name = 'event list';
INSERT INTO `command` (`name` ,`security` ,`help`) VALUES
('event list', '1', 'Syntax: .event list Liste des events utilisables par les Mj.');

DELETE FROM `trinity_string` WHERE `entry` BETWEEN 12106 AND 12109;
INSERT INTO `trinity_string` (`entry` ,`content_default`) VALUES
('12106', '%d | %s | %s'),
('12107', '|c00077766ON'),
('12108', '|cffff0000OFF'),
('12109', 'Aucun GameEvent � lister...');

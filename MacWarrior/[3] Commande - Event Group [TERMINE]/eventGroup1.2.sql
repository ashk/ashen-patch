DELETE FROM `trinity_string` WHERE `entry` BETWEEN 12100 AND 12105;
INSERT INTO `trinity_string` (`entry` ,`content_default`) VALUES
('12100', 'Vous n''êtes pas dans un groupe'),
('12101', 'La commande est déjà activée'),
('12102', 'La commande est déjà désactivée'),
('12103', 'Seuls le leader et l''assistant du groupe peuvent utiliser cette commande'),
('12104', 'Les membres du groupe ne peuvent plus utiliser leurs sorts'),
('12105', 'Les membres du groupe peuvent à nouveau utiliser leurs sorts');

DELETE FROM command WHERE name IN ('event group start','event group stop');
INSERT INTO `command` (`name` ,`security` ,`help`) VALUES
('event group start', '1', 'Syntax: .event group active Désactive les sorts des membres du groupe.'),
('event group stop', '1', 'Syntax: .event group desactive Réactive les sorts des membres du groupe.');

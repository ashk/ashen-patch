Description du syst�me :

- Ce script a pour but de permettre au joueur durant un �vent parcours de revenir � un check point si il tombe du parcours, ou s'en �chappe.
- A chaque d�but d'�vent la Pierre Check Point est mise en vente aux joueurs.
- Au fur et � mesure que l'on avance dans le parcours, on rencontre des PNJ Check Point, quand le joueur lui parle, sa position est enregistr�e dans l'item.
- Si le joueur clique sur l'item apr�s avoir enregistr� sa position il sera t�l�port� � cette derni�re.

Avantage : 

- On peut utilis� le m�me PNJ (entry) et le m�me item (entry) pour chaque �vent
- On ne modifie pas de sort d�j� existant (spell_target_position)

V1 : 
Utilisation d'une table dans la BDD characters

V2 : 
Utilisation d'une variable de class

###################################
#        Ce qui est fait :        #
###################################

BDD (V1 uniquement): 
Ajout d'une table 'item_event_teleport' � la BDD 'character'
PNJ : (ID dans le sql fourni : 100000)
Un PNJ type gossip auquel est li� le script.
Lorsque le joueur 'croise' le PNJ, il peut alors y lier sa 'pierre de t�l�portation'.
Le checkpoint reste actif pendant 1h apr�s activation, le joueur devra alors r�-activer le checkpoint.
ITEM : (ID dans le sql fourni : 100000)
Si le joueur a li� l'item � un checkpoint, il peut alors l'utiliser pour s'y t�l�porter.

###################################
#  Temps de travail actuel: 4h00  #
###################################

Patch adapt� � la derni�re r�vision de Trinity - 13/09/2012:12h30
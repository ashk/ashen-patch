#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include <cstring>

class npc_event_teleport : public CreatureScript
{
    public:

        npc_event_teleport(): CreatureScript("npc_event_teleport"){}

		bool OnGossipHello(Player* player, Creature* creature)
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Checkpoint !", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
			player->PlayerTalkClass->SendGossipMenu(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
			return true;
		}

		bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
		{
			player->PlayerTalkClass->ClearMenus();
			if (action == GOSSIP_ACTION_INFO_DEF+1)
			{
				time_t t;
				time(&t);
				player->SetCheckpoint((uint32)t, player->GetPositionX(), player->GetPositionY(), player->GetPositionZ(), player->GetOrientation(), player->GetMapId());
				creature->MonsterWhisper(player->GetSession()->GetTrinityString(LANG_STRING_EVENT_TELEPORT_CHECKPOINT_CHECKED), player->GetGUID());
				player->CLOSE_GOSSIP_MENU();
            }
            return true;
        }
};

void AddSC_npc_event_teleport()
{
    new npc_event_teleport();
}
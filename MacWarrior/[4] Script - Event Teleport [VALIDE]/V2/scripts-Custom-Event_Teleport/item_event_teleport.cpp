#include "ScriptMgr.h"

class item_event_teleport : public ItemScript
{
public:
    item_event_teleport() : ItemScript("item_event_teleport") { }

    bool OnUse(Player* player, Item* /*item*/, SpellCastTargets const& /*targets*/)
    {
		float* tab = player->GetCheckpoint();
		if( tab[0] == NULL )
			player->SetCheckpoint(0, 0, 0, 0, 0, 0);

		time_t t;
		time(&t);

		if( (uint32)tab[0] == 0 || (uint32)tab[0] < (uint32)t-3600)
		{
			ChatHandler(player).SendSysMessage(player->GetSession()->GetTrinityString(LANG_SCRIPT_EVENT_TELEPORT_NO_CHECKPOINT));
			return true;
		}

		player->TeleportTo((uint32)tab[5], tab[1], tab[2], tab[3], tab[4]);
        return true;
    }
};

void AddSC_item_event_teleport()
{
    new item_event_teleport();
}
/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"

/* Item event teleport - By MacWarrior */
class item_event_teleport : public ItemScript
{
public:
    item_event_teleport() : ItemScript("item_event_teleport") { }

    bool OnUse(Player* player, Item* /*item*/, SpellCastTargets const& /*targets*/)
    {
		uint32 accountId = player->GetSession()->GetAccountId();
		QueryResult result = CharacterDatabase.PQuery("SELECT timestamp, coord_map, coord_x, coord_y, coord_z, coord_o FROM item_event_teleport WHERE id = %d", accountId);
		if (!result)
		{
			ChatHandler(player).SendSysMessage(player->GetSession()->GetTrinityString(LANG_SCRIPT_EVENT_TELEPORT_NO_CHECKPOINT));
			return true;
		}

		Field* fields		= result->Fetch();
		uint32 timestamp	= fields[0].GetUInt32();
		uint32 coord_map	= fields[1].GetUInt32();
		float coord_x		= fields[2].GetFloat();
		float coord_y		= fields[3].GetFloat();
		float coord_z		= fields[4].GetFloat();
		float coord_o		= fields[5].GetFloat();

		time_t t;
		time(&t);

		if( timestamp < (uint32)t-3600 )
		{
			CharacterDatabase.PExecute("DELETE FROM item_event_teleport WHERE id = %d", accountId);
			ChatHandler(player).SendSysMessage(player->GetSession()->GetTrinityString(LANG_SCRIPT_EVENT_TELEPORT_NO_CHECKPOINT));
			return true;
		}

		player->InterruptNonMeleeSpells(false);
		player->TeleportTo(coord_map, coord_x, coord_y, coord_z+5, coord_o);
        return true;
    }
};

void AddSC_item_event_teleport()
{
    new item_event_teleport();
}
/* Item event teleport - By MacWarrior */

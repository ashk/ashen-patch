/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include <cstring>

/* Item event teleport - By MacWarrior */
class npc_event_teleport : public CreatureScript
{
    public:

        npc_event_teleport(): CreatureScript("npc_event_teleport"){}

		bool OnGossipHello(Player* player, Creature* creature)
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Checkpoint !", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
			player->PlayerTalkClass->SendGossipMenu(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
			return true;
		}

		bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
		{
			player->PlayerTalkClass->ClearMenus();
			if (action == GOSSIP_ACTION_INFO_DEF+1)
			{
				time_t t;
				time(&t);
				uint32 accountId = player->GetSession()->GetAccountId();
				CharacterDatabase.PExecute("DELETE FROM item_event_teleport WHERE id = %d", accountId);
				CharacterDatabase.PExecute("INSERT INTO item_event_teleport (id, timestamp, coord_map, coord_x, coord_y, coord_z, coord_o) VALUES (%d, %d, %d, %f, %f, %f, %f)", accountId, (uint32)t, player->GetMapId(), player->GetPositionX(), player->GetPositionY(), player->GetPositionZ(), player->GetOrientation());
				creature->MonsterWhisper(player->GetSession()->GetTrinityString(LANG_STRING_EVENT_TELEPORT_CHECKPOINT_CHECKED), player->GetGUID());
				player->CLOSE_GOSSIP_MENU();
            }
            return true;
        }
};

void AddSC_npc_event_teleport()
{
    new npc_event_teleport();
}
/* Item event teleport - By MacWarrior */

SET @IdGameEvent = 80;
/* Attention aux ID, Trinity ne supporte pas les eventEntry > 128 */

SET @TempsVagueAttaque = 3;
SET @TempsVagueDefense = 3;
Set @TempsPriseCamp = 30;
/* Attention aux PNJ qui se chevaucheront si le temps est modifi� */

/* La colonne event_list corresponds � la commande .event list ( Cf. Patch custom ) */
INSERT INTO `game_event` (`eventEntry`, `start_time`, `end_time`, `occurence`, `length`, `holiday`, `description`, `world_event`, `event_list`) VALUES
(@IdGameEvent, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueAttaque, 0, '[AttaqueCapitales-Alliance] Vague 1', 1, 1),
(@IdGameEvent+1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, 2, 0, '[AttaqueCapitales-Alliance] Defense 1', 1, 1),
(@IdGameEvent+2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueAttaque, 0, '[AttaqueCapitales-Alliance] Vague 2', 1, 1),
(@IdGameEvent+3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueAttaque, 0, '[AttaqueCapitales-Alliance] Vague 3', 1, 1),
(@IdGameEvent+4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, 5, 0, '[AttaqueCapitales-Alliance] Defense 2', 1, 1),
(@IdGameEvent+5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueAttaque, 0, '[AttaqueCapitales-Alliance] Vague 4', 1, 1),
(@IdGameEvent+6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsPriseCamp, 0, '[AttaqueCapitales-Alliance] Prise du camp', 1, 1),
(@IdGameEvent+7, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueAttaque, 0, '[AttaqueCapitales-Alliance] Vague 5', 1, 1),
(@IdGameEvent+8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, 2, 0, '[AttaqueCapitales-Alliance] Portail Hurlevent', 1, 1),
(@IdGameEvent+9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueDefense, 0, '[AttaqueCapitales-Alliance] Vague D�fense 1', 1, 1),
(@IdGameEvent+10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueDefense, 0, '[AttaqueCapitales-Alliance] Vague D�fense 2', 1, 1),
(@IdGameEvent+11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, 4, 0, '[AttaqueCapitales-Alliance] Eveil chef A2', 1, 1),
(@IdGameEvent+12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, 4, 0, '[AttaqueCapitales-Alliance] Mise en place arm�e', 1, 1),
(@IdGameEvent+13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, 10, 0, '[AttaqueCapitales-Alliance] Attaque arm�e', 1, 1),
(@IdGameEvent+14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueDefense, 0, '[AttaqueCapitales-Alliance] Vague D�fense 3', 1, 1),
(@IdGameEvent+15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueDefense, 0, '[AttaqueCapitales-Alliance] Vague D�fense 4', 1, 1),
(@IdGameEvent+16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueDefense, 0, '[AttaqueCapitales-Alliance] Vague D�fense 5', 1, 1),
(@IdGameEvent+17, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueAttaque, 0, '[AttaqueCapitales-Horde] Vague 1', 1, 1),
(@IdGameEvent+18, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsPriseCamp, 0, '[AttaqueCapitales-Horde] Prise du camp', 1, 1),
(@IdGameEvent+19, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueAttaque, 0, '[AttaqueCapitales-Horde] Vague 2', 1, 1),
(@IdGameEvent+20, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueAttaque, 0, '[AttaqueCapitales-Horde] Vague 3', 1, 1),
(@IdGameEvent+21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueAttaque, 0, '[AttaqueCapitales-Horde] Vague 4', 1, 1),
(@IdGameEvent+22, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueAttaque, 0, '[AttaqueCapitales-Horde] Vague 5', 1, 1),
(@IdGameEvent+23, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueDefense, 0, '[AttaqueCapitales-Horde] Vague D�fense 1', 1, 1),
(@IdGameEvent+24, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueDefense, 0, '[AttaqueCapitales-Horde] Vague D�fense 2', 1, 1),
(@IdGameEvent+25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueDefense, 0, '[AttaqueCapitales-Horde] Vague D�fense 3', 1, 1),
(@IdGameEvent+26, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueDefense, 0, '[AttaqueCapitales-Horde] Vague D�fense 4', 1, 1),
(@IdGameEvent+27, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, @TempsVagueDefense, 0, '[AttaqueCapitales-Horde] Vague D�fense 5', 1, 1),
(@IdGameEvent+28, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, 4, 0, '[AttaqueCapitales-Horde] Eveil chef H2', 1, 1),
(@IdGameEvent+29, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, 4, 0, '[AttaqueCapitales-Horde] Mise en place arm�e', 1, 1),
(@IdGameEvent+30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 525600, 10, 0, '[AttaqueCapitales-Horde] Attaque arm�e', 1, 1);

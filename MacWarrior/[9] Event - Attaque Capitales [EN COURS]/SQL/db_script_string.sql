SET @IdDbScriptString = 2000006000;

INSERT INTO `db_script_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`) VALUES
(@IdDbScriptString, 'La Horde nous attaque, au combat !', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(@IdDbScriptString+1, 'Peuple de l''Alliance, la Horde a captur� notre camp ! Repliez-vous !', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(@IdDbScriptString+2, 'Mort...Je suis... mort...La horde paiera pour son insolence !', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(@IdDbScriptString+3, 'Peuple de l''Alliance, notre campement a �t� souill� par la Horde, montrons leur notre puissance et reprenons notre place en tant que ma�tre d''Azeroth !', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(@IdDbScriptString+4, 'Soldats, pr�parez-vous au combat, la Horde n''attends plus que vous !', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(@IdDbScriptString+5, 'Partez maintenant au combat, braves soldats de l''Alliance !', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

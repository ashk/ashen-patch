SET @IdGameEvent = 80;
/* Attention l'ID doit correspondre � celui pr�sent dans game_event.sql */

SET @GuidCreature = 210000;
/* Attention l'ID doit correspondre � celui pr�sent dans creature.sql */

INSERT INTO `game_event_creature` (`eventEntry`, `guid`) VALUES
(@IdGameEvent, @GuidCreature+31),
(@IdGameEvent, @GuidCreature+45),
(@IdGameEvent, @GuidCreature+73),
(@IdGameEvent, @GuidCreature+30),
(@IdGameEvent, @GuidCreature+29),
(@IdGameEvent+1, @GuidCreature+71),
(@IdGameEvent+2, @GuidCreature+26),
(@IdGameEvent+2, @GuidCreature+27),
(@IdGameEvent+2, @GuidCreature+28),
(@IdGameEvent+2, @GuidCreature+72),
(@IdGameEvent+3, @GuidCreature+42),
(@IdGameEvent+3, @GuidCreature+43),
(@IdGameEvent+3, @GuidCreature+44),
(@IdGameEvent+3, @GuidCreature+41),
(@IdGameEvent+3, @GuidCreature+40),
(@IdGameEvent+4, @GuidCreature+76),
(@IdGameEvent+4, @GuidCreature+78),
(@IdGameEvent+4, @GuidCreature+79),
(@IdGameEvent+4, @GuidCreature+80),
(@IdGameEvent+4, @GuidCreature+81),
(@IdGameEvent+4, @GuidCreature+75),
(@IdGameEvent+4, @GuidCreature+77),
(@IdGameEvent+4, @GuidCreature+74),
(@IdGameEvent+5, @GuidCreature+37),
(@IdGameEvent+5, @GuidCreature+39),
(@IdGameEvent+5, @GuidCreature+38),
(@IdGameEvent+5, @GuidCreature+33),
(@IdGameEvent+6, @GuidCreature+181),
(@IdGameEvent+6, @GuidCreature+105),
(@IdGameEvent+6, @GuidCreature+104),
(@IdGameEvent+6, @GuidCreature+103),
(@IdGameEvent+6, @GuidCreature+102),
(@IdGameEvent+6, @GuidCreature+101),
(@IdGameEvent+6, @GuidCreature+100),
(@IdGameEvent+6, @GuidCreature+169),
(@IdGameEvent+6, @GuidCreature+209),
(@IdGameEvent+6, @GuidCreature+208),
(@IdGameEvent+6, @GuidCreature+185),
(@IdGameEvent+6, @GuidCreature+183),
(@IdGameEvent+6, @GuidCreature+172),
(@IdGameEvent+6, @GuidCreature+171),
(@IdGameEvent+6, @GuidCreature+167),
(@IdGameEvent+6, @GuidCreature+168),
(@IdGameEvent+6, @GuidCreature+170),
(@IdGameEvent+6, @GuidCreature+99),
(@IdGameEvent+6, @GuidCreature+98),
(@IdGameEvent+6, @GuidCreature+97),
(@IdGameEvent+6, @GuidCreature+86),
(@IdGameEvent+6, @GuidCreature+85),
(@IdGameEvent+6, @GuidCreature+84),
(@IdGameEvent+6, @GuidCreature+83),
(@IdGameEvent+6, @GuidCreature+88),
(@IdGameEvent+6, @GuidCreature+90),
(@IdGameEvent+6, @GuidCreature+89),
(@IdGameEvent+6, @GuidCreature+91),
(@IdGameEvent+6, @GuidCreature+92),
(@IdGameEvent+6, @GuidCreature+93),
(@IdGameEvent+6, @GuidCreature+94),
(@IdGameEvent+6, @GuidCreature+95),
(@IdGameEvent+6, @GuidCreature+96),
(@IdGameEvent+6, @GuidCreature+87),
(@IdGameEvent+7, @GuidCreature+35),
(@IdGameEvent+7, @GuidCreature+34),
(@IdGameEvent+7, @GuidCreature+32),
(@IdGameEvent+8, @GuidCreature+118),
(@IdGameEvent+8, @GuidCreature+117),
(@IdGameEvent+8, @GuidCreature+106),
(@IdGameEvent+9, @GuidCreature+107),
(@IdGameEvent+9, @GuidCreature+108),
(@IdGameEvent+9, @GuidCreature+109),
(@IdGameEvent+9, @GuidCreature+110),
(@IdGameEvent+9, @GuidCreature+111),
(@IdGameEvent+10, @GuidCreature+115),
(@IdGameEvent+10, @GuidCreature+112),
(@IdGameEvent+10, @GuidCreature+113),
(@IdGameEvent+10, @GuidCreature+114),
(@IdGameEvent+10, @GuidCreature+116),
(@IdGameEvent+11, @GuidCreature+119),
(@IdGameEvent+12, @GuidCreature+120),
(@IdGameEvent+12, @GuidCreature+140),
(@IdGameEvent+12, @GuidCreature+139),
(@IdGameEvent+12, @GuidCreature+138),
(@IdGameEvent+12, @GuidCreature+136),
(@IdGameEvent+12, @GuidCreature+135),
(@IdGameEvent+12, @GuidCreature+134),
(@IdGameEvent+12, @GuidCreature+128),
(@IdGameEvent+12, @GuidCreature+127),
(@IdGameEvent+12, @GuidCreature+126),
(@IdGameEvent+12, @GuidCreature+125),
(@IdGameEvent+12, @GuidCreature+124),
(@IdGameEvent+12, @GuidCreature+123),
(@IdGameEvent+12, @GuidCreature+121),
(@IdGameEvent+12, @GuidCreature+129),
(@IdGameEvent+12, @GuidCreature+130),
(@IdGameEvent+12, @GuidCreature+131),
(@IdGameEvent+12, @GuidCreature+132),
(@IdGameEvent+12, @GuidCreature+133),
(@IdGameEvent+12, @GuidCreature+122),
(-(@IdGameEvent+13), 79677),
(@IdGameEvent+13, @GuidCreature+159),
(@IdGameEvent+13, @GuidCreature+160),
(@IdGameEvent+13, @GuidCreature+142),
(@IdGameEvent+13, @GuidCreature+141),
(@IdGameEvent+13, @GuidCreature+155),
(@IdGameEvent+13, @GuidCreature+158),
(@IdGameEvent+13, @GuidCreature+157),
(@IdGameEvent+13, @GuidCreature+156),
(@IdGameEvent+13, @GuidCreature+143),
(@IdGameEvent+13, @GuidCreature+137),
(@IdGameEvent+13, @GuidCreature+144),
(@IdGameEvent+13, @GuidCreature+145),
(@IdGameEvent+13, @GuidCreature+146),
(@IdGameEvent+13, @GuidCreature+147),
(@IdGameEvent+13, @GuidCreature+148),
(@IdGameEvent+13, @GuidCreature+149),
(@IdGameEvent+13, @GuidCreature+150),
(@IdGameEvent+13, @GuidCreature+151),
(@IdGameEvent+13, @GuidCreature+152),
(@IdGameEvent+13, @GuidCreature+153),
(@IdGameEvent+13, @GuidCreature+154),
(@IdGameEvent+14, @GuidCreature+192),
(@IdGameEvent+14, @GuidCreature+191),
(@IdGameEvent+14, @GuidCreature+190),
(@IdGameEvent+14, @GuidCreature+189),
(@IdGameEvent+14, @GuidCreature+187),
(@IdGameEvent+14, @GuidCreature+188),
(@IdGameEvent+15, @GuidCreature+198),
(@IdGameEvent+15, @GuidCreature+197),
(@IdGameEvent+15, @GuidCreature+196),
(@IdGameEvent+15, @GuidCreature+195),
(@IdGameEvent+15, @GuidCreature+194),
(@IdGameEvent+15, @GuidCreature+193),
(@IdGameEvent+16, @GuidCreature+207),
(@IdGameEvent+16, @GuidCreature+206),
(@IdGameEvent+16, @GuidCreature+205),
(@IdGameEvent+16, @GuidCreature+204),
(@IdGameEvent+16, @GuidCreature+203),
(@IdGameEvent+16, @GuidCreature+202),
(@IdGameEvent+16, @GuidCreature+201),
(@IdGameEvent+16, @GuidCreature+200),
(@IdGameEvent+16, @GuidCreature+199),
(@IdGameEvent+17, @GuidCreature+252),
(@IdGameEvent+17, @GuidCreature+251),
(@IdGameEvent+17, @GuidCreature+250),
(@IdGameEvent+17, @GuidCreature+249),
(-(@IdGameEvent+18), @GuidCreature+240),
(-(@IdGameEvent+18), @GuidCreature+241),
(-(@IdGameEvent+18), @GuidCreature+242),
(-(@IdGameEvent+18), @GuidCreature+243),
(-(@IdGameEvent+18), @GuidCreature+266),
(-(@IdGameEvent+18), @GuidCreature+267),
(-(@IdGameEvent+18), @GuidCreature+268),
(-(@IdGameEvent+18), @GuidCreature+269),
(-(@IdGameEvent+18), @GuidCreature+239),
(-(@IdGameEvent+18), @GuidCreature+237),
(-(@IdGameEvent+18), @GuidCreature+229),
(-(@IdGameEvent+18), @GuidCreature+230),
(-(@IdGameEvent+18), @GuidCreature+231),
(-(@IdGameEvent+18), @GuidCreature+232),
(-(@IdGameEvent+18), @GuidCreature+233),
(-(@IdGameEvent+18), @GuidCreature+234),
(-(@IdGameEvent+18), @GuidCreature+235),
(-(@IdGameEvent+18), @GuidCreature+236),
(-(@IdGameEvent+18), @GuidCreature+270),
(-(@IdGameEvent+18), @GuidCreature+295),
(-(@IdGameEvent+18), @GuidCreature+307),
(-(@IdGameEvent+18), @GuidCreature+308),
(-(@IdGameEvent+18), @GuidCreature+309),
(-(@IdGameEvent+18), @GuidCreature+310),
(-(@IdGameEvent+18), @GuidCreature+311),
(-(@IdGameEvent+18), @GuidCreature+312),
(-(@IdGameEvent+18), @GuidCreature+313),
(-(@IdGameEvent+18), @GuidCreature+314),
(-(@IdGameEvent+18), @GuidCreature+306),
(-(@IdGameEvent+18), @GuidCreature+305),
(-(@IdGameEvent+18), @GuidCreature+296),
(-(@IdGameEvent+18), @GuidCreature+297),
(-(@IdGameEvent+18), @GuidCreature+298),
(-(@IdGameEvent+18), @GuidCreature+299),
(-(@IdGameEvent+18), @GuidCreature+300),
(-(@IdGameEvent+18), @GuidCreature+301),
(-(@IdGameEvent+18), @GuidCreature+302),
(-(@IdGameEvent+18), @GuidCreature+303),
(-(@IdGameEvent+18), @GuidCreature+228),
(-(@IdGameEvent+18), @GuidCreature+227),
(-(@IdGameEvent+18), @GuidCreature+226),
(-(@IdGameEvent+18), @GuidCreature+238),
(-(@IdGameEvent+18), @GuidCreature+304),
(-(@IdGameEvent+18), @GuidCreature+220),
(-(@IdGameEvent+18), @GuidCreature+221),
(-(@IdGameEvent+18), @GuidCreature+222),
(-(@IdGameEvent+18), @GuidCreature+223),
(-(@IdGameEvent+18), @GuidCreature+224),
(-(@IdGameEvent+18), @GuidCreature+225),
(@IdGameEvent+18, @GuidCreature+333),
(@IdGameEvent+18, @GuidCreature+345),
(@IdGameEvent+18, @GuidCreature+344),
(@IdGameEvent+18, @GuidCreature+343),
(@IdGameEvent+18, @GuidCreature+342),
(@IdGameEvent+18, @GuidCreature+341),
(@IdGameEvent+18, @GuidCreature+340),
(@IdGameEvent+18, @GuidCreature+339),
(@IdGameEvent+18, @GuidCreature+338),
(@IdGameEvent+18, @GuidCreature+337),
(@IdGameEvent+18, @GuidCreature+336),
(@IdGameEvent+18, @GuidCreature+331),
(@IdGameEvent+18, @GuidCreature+332),
(@IdGameEvent+18, @GuidCreature+335),
(@IdGameEvent+18, @GuidCreature+334),
(@IdGameEvent+18, @GuidCreature+346),
(@IdGameEvent+18, @GuidCreature+347),
(@IdGameEvent+18, @GuidCreature+348),
(@IdGameEvent+18, @GuidCreature+361),
(@IdGameEvent+18, @GuidCreature+360),
(@IdGameEvent+18, @GuidCreature+359),
(@IdGameEvent+18, @GuidCreature+363),
(@IdGameEvent+18, @GuidCreature+358),
(@IdGameEvent+18, @GuidCreature+357),
(@IdGameEvent+18, @GuidCreature+356),
(@IdGameEvent+18, @GuidCreature+355),
(@IdGameEvent+18, @GuidCreature+354),
(@IdGameEvent+18, @GuidCreature+353),
(@IdGameEvent+18, @GuidCreature+352),
(@IdGameEvent+18, @GuidCreature+351),
(@IdGameEvent+18, @GuidCreature+350),
(@IdGameEvent+18, @GuidCreature+349),
(@IdGameEvent+18, @GuidCreature+362),
(@IdGameEvent+18, @GuidCreature+317),
(@IdGameEvent+18, @GuidCreature+330),
(@IdGameEvent+18, @GuidCreature+315),
(@IdGameEvent+18, @GuidCreature+316),
(@IdGameEvent+18, @GuidCreature+318),
(@IdGameEvent+18, @GuidCreature+319),
(@IdGameEvent+18, @GuidCreature+320),
(@IdGameEvent+18, @GuidCreature+321),
(@IdGameEvent+18, @GuidCreature+322),
(@IdGameEvent+18, @GuidCreature+323),
(@IdGameEvent+18, @GuidCreature+329),
(@IdGameEvent+18, @GuidCreature+328),
(@IdGameEvent+18, @GuidCreature+327),
(@IdGameEvent+18, @GuidCreature+326),
(@IdGameEvent+18, @GuidCreature+325),
(@IdGameEvent+18, @GuidCreature+324),
(@IdGameEvent+19, @GuidCreature+248),
(@IdGameEvent+19, @GuidCreature+246),
(@IdGameEvent+19, @GuidCreature+244),
(@IdGameEvent+19, @GuidCreature+247),
(@IdGameEvent+19, @GuidCreature+245),
(@IdGameEvent+20, @GuidCreature+263),
(@IdGameEvent+20, @GuidCreature+265),
(@IdGameEvent+20, @GuidCreature+262),
(@IdGameEvent+20, @GuidCreature+264),
(@IdGameEvent+21, @GuidCreature+256),
(@IdGameEvent+21, @GuidCreature+257),
(@IdGameEvent+21, @GuidCreature+254),
(@IdGameEvent+21, @GuidCreature+253),
(@IdGameEvent+21, @GuidCreature+255),
(@IdGameEvent+22, @GuidCreature+260),
(@IdGameEvent+22, @GuidCreature+259),
(@IdGameEvent+22, @GuidCreature+261),
(@IdGameEvent+23, @GuidCreature+368),
(@IdGameEvent+23, @GuidCreature+367),
(@IdGameEvent+23, @GuidCreature+366),
(@IdGameEvent+23, @GuidCreature+365),
(@IdGameEvent+23, @GuidCreature+364),
(@IdGameEvent+24, @GuidCreature+377),
(@IdGameEvent+24, @GuidCreature+376),
(@IdGameEvent+24, @GuidCreature+375),
(@IdGameEvent+24, @GuidCreature+374),
(@IdGameEvent+24, @GuidCreature+373),
(@IdGameEvent+24, @GuidCreature+372),
(@IdGameEvent+24, @GuidCreature+371),
(@IdGameEvent+24, @GuidCreature+370),
(@IdGameEvent+24, @GuidCreature+369),
(@IdGameEvent+25, @GuidCreature+382),
(@IdGameEvent+25, @GuidCreature+381),
(@IdGameEvent+25, @GuidCreature+380),
(@IdGameEvent+25, @GuidCreature+379),
(@IdGameEvent+25, @GuidCreature+378),
(-(@IdGameEvent+26), @GuidCreature+9),
(-(@IdGameEvent+26), @GuidCreature+10),
(-(@IdGameEvent+26), @GuidCreature+11),
(-(@IdGameEvent+26), @GuidCreature+12),
(-(@IdGameEvent+26), @GuidCreature+17),
(-(@IdGameEvent+26), @GuidCreature+8),
(-(@IdGameEvent+26), @GuidCreature+7),
(-(@IdGameEvent+26), @GuidCreature+6),
(-(@IdGameEvent+26), @GuidCreature+5),
(-(@IdGameEvent+26), @GuidCreature+4),
(-(@IdGameEvent+26), @GuidCreature+3),
(-(@IdGameEvent+26), @GuidCreature+2),
(-(@IdGameEvent+26), @GuidCreature+1),
(-(@IdGameEvent+26), @GuidCreature),
(-(@IdGameEvent+26), @GuidCreature+18),
(-(@IdGameEvent+26), @GuidCreature+19),
(-(@IdGameEvent+26), @GuidCreature+20),
(-(@IdGameEvent+26), @GuidCreature+182),
(-(@IdGameEvent+26), @GuidCreature+184),
(-(@IdGameEvent+26), @GuidCreature+166),
(-(@IdGameEvent+26), @GuidCreature+165),
(-(@IdGameEvent+26), @GuidCreature+164),
(-(@IdGameEvent+26), @GuidCreature+163),
(-(@IdGameEvent+26), @GuidCreature+162),
(-(@IdGameEvent+26), @GuidCreature+161),
(-(@IdGameEvent+26), @GuidCreature+82),
(-(@IdGameEvent+26), @GuidCreature+25),
(-(@IdGameEvent+26), @GuidCreature+24),
(-(@IdGameEvent+26), @GuidCreature+23),
(-(@IdGameEvent+26), @GuidCreature+22),
(-(@IdGameEvent+26), @GuidCreature+21),
(-(@IdGameEvent+26), @GuidCreature+186),
(@IdGameEvent+26, @GuidCreature+388),
(@IdGameEvent+26, @GuidCreature+387),
(@IdGameEvent+26, @GuidCreature+386),
(@IdGameEvent+26, @GuidCreature+385),
(@IdGameEvent+26, @GuidCreature+384),
(@IdGameEvent+26, @GuidCreature+383),
(@IdGameEvent+27, @GuidCreature+420),
(@IdGameEvent+27, @GuidCreature+419),
(@IdGameEvent+27, @GuidCreature+394),
(@IdGameEvent+27, @GuidCreature+393),
(@IdGameEvent+27, @GuidCreature+392),
(@IdGameEvent+27, @GuidCreature+391),
(@IdGameEvent+27, @GuidCreature+390),
(@IdGameEvent+27, @GuidCreature+389),
(@IdGameEvent+29, @GuidCreature+422),
(@IdGameEvent+29, @GuidCreature+420),
(@IdGameEvent+29, @GuidCreature+419),
(@IdGameEvent+29, @GuidCreature+418),
(@IdGameEvent+29, @GuidCreature+416),
(@IdGameEvent+29, @GuidCreature+423),
(@IdGameEvent+29, @GuidCreature+424),
(@IdGameEvent+29, @GuidCreature+425),
(@IdGameEvent+29, @GuidCreature+426),
(@IdGameEvent+29, @GuidCreature+427),
(@IdGameEvent+29, @GuidCreature+429),
(@IdGameEvent+29, @GuidCreature+430),
(@IdGameEvent+29, @GuidCreature+431),
(@IdGameEvent+29, @GuidCreature+432),
(@IdGameEvent+29, @GuidCreature+433),
(@IdGameEvent+29, @GuidCreature+415),
(@IdGameEvent+29, @GuidCreature+417),
(@IdGameEvent+29, @GuidCreature+434),
(@IdGameEvent+29, @GuidCreature+428),
(@IdGameEvent+29, @GuidCreature+421),
(@IdGameEvent+30, @GuidCreature+413),
(@IdGameEvent+30, @GuidCreature+400),
(@IdGameEvent+30, @GuidCreature+411),
(@IdGameEvent+30, @GuidCreature+399),
(@IdGameEvent+30, @GuidCreature+398),
(@IdGameEvent+30, @GuidCreature+397),
(@IdGameEvent+30, @GuidCreature+396),
(@IdGameEvent+30, @GuidCreature+395),
(@IdGameEvent+30, @GuidCreature+402),
(@IdGameEvent+30, @GuidCreature+403),
(@IdGameEvent+30, @GuidCreature+404),
(@IdGameEvent+30, @GuidCreature+414),
(@IdGameEvent+30, @GuidCreature+412),
(@IdGameEvent+30, @GuidCreature+405),
(@IdGameEvent+30, @GuidCreature+409),
(@IdGameEvent+30, @GuidCreature+408),
(@IdGameEvent+30, @GuidCreature+407),
(@IdGameEvent+30, @GuidCreature+406),
(@IdGameEvent+30, @GuidCreature+410),
(@IdGameEvent+30, @GuidCreature+401);

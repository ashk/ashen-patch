/*
SQLyog Enterprise - MySQL GUI v8.18 
MySQL - 5.1.63-0+squeeze1 : Database - world
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `creature_reward_code` */

DROP TABLE IF EXISTS `creature_reward_code`;

CREATE TABLE `creature_reward_code` (
  `code` varchar(255) NOT NULL,
  `max_use` int(8) NOT NULL,
  `description` varchar(255) NOT NULL,
  `auto_delete` int(1) NOT NULL DEFAULT '0',
  `type` int(255) NOT NULL DEFAULT '0',
  `parameter` bigint(255) NOT NULL DEFAULT '0',
  `use_message` varchar(255) NOT NULL,
  `account` bigint(255) NOT NULL DEFAULT '-1',
  `date` bigint(255) NOT NULL,
  `shopItem` int(11) DEFAULT '0',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

/*
SQLyog Enterprise - MySQL GUI v8.18 
MySQL - 5.1.63-0+squeeze1 : Database - characters
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `character_reward_code_history` */

DROP TABLE IF EXISTS `character_reward_code_history`;

CREATE TABLE `character_reward_code_history` (
  `character` bigint(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `use` int(8) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date` bigint(255) NOT NULL,
  `characterName` varchar(255) NOT NULL,
  `account` bigint(255) DEFAULT NULL,
  PRIMARY KEY (`character`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
